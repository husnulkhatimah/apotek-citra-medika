<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Apotik</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Apotek Citra Medika</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="user">HOME USER</a>
        <a class="nav-link" href="{{url('hh')}}">DIAGNOSA</a>
      </div>
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="#"></a>
        <a class="nav-link" href="{{url('bbb')}}">DAFTAR OBAT</a>
      </div>
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="#"></a>
        <a class="nav-link" href="{{url('cc')}}">PESANAN</a>
      </div>
      
    </div>
  </div>
</nav>
<h2> APOTEK CITRA MEDIKA FARMA</h2>
<div class="card-body">
    <table class="table table-bordered">
        <tr>
            <th>id</th>
            <th>Kode Obat</th>
            <th>Nama Obat</th>
            <th>Bentuk</th>
            <th>Konsumen</th>
            <th>Harga</th>
            
        </tr>
        @foreach ($dtpesan as $item)
        <tr>
            <td>{{ $item->id}}</td>
            <td>{{ $item->kode_obat}}</td>
            <td>{{ $item->nama_obat}}</td>
            <td>{{ $item->bentuk_obat}}</td>
            <td>{{ $item->konsumen}}</td>
            <td>{{ $item->harga}}</td>
            
        </tr>
        @endforeach
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </table>
    
</div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>