<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Apotik</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Apotik Citra Medika</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="home">HOME</a>
            <a class="nav-link" href="{{url('pesan')}}">DATA OBAT</a>
          </div>
          <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="#"></a>
            <a class="nav-link" href="{{url('stok1')}}">STOK OBAT</a>
          </div>
          <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="#"></a>
            <a class="nav-link" href="{{url('diagnosa')}}">DIAGNOSA PENYAKIT</a>
          </div>
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="#"></a>
        <a class="nav-link" href="{{url('laporan')}}">LAPORAN TRANSAKSI PENJUAL</a>
      </div>
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="#"></a>
      </div>
      <form action="/logout" method="POST" >
        @csrf
          <button type="submit" class=""> Logout</button>
       </form>
    </div>
  </div>
</nav>
  <h2> APOTEK CITRA MEDIKA FARMA</h2>
    <section>
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="alert alert-primary text-center mt-3">Edit Diagnosa</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('edit3',$data->id)}}" method="POST">
                        @csrf
                        @method('put')
                        <div class="from-group mt-2">
                            <input type="text" id=id name="id" class="form-control" placeholder="Id"  value="{{$data->id}}">
                        </div>
                        <div class="from-group mt-2">
                            <input type="date" id=tanggal name="tanggal" class="form-control" placeholder="Tanggal" value="{{$data->tanggal}}">
                        </div>
                        <div class="from-group mt-2">
                            <input type="text" id=nama name="nama_obat" class="form-control" placeholder="Nama Obat" value="{{$data->nama_obat}}">
                        </div>
                        <div class="from-group mt-2">
                            <input type="text" id=stok name="stok_terjual" class="form-control" placeholder="Stok Terjual" value="{{$data->stok_terjual}}">
                        </div>
                        <div class="from-group mt-2">
                          <input type="text" id=jumlah name="jumlah" class="form-control" placeholder="Jumlah" value="{{$data->jumlah}}">
                        </div>
                        <div class="from-group mt-2">
                            <button type="submit" class="btn btn-primary">edit laporan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>