<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Apotik</title>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        <a class="navbar-brand" href="home">Apotek Citra Medika</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="home">HOME</a>
            <a class="nav-link" href="{{url('pesan')}}">DATA OBAT</a>
          </div>
          <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="#"></a>
            <a class="nav-link" href="{{url('stok1')}}">STOK OBAT</a>
          </div>
          <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="#"></a>
            <a class="nav-link" href="{{url('diagnosa')}}">DIAGNOSA PENYAKIT</a>
          </div>
          <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="#"></a>
            <a class="nav-link" href="laporan">LAPORAN TRANSAKSI PENJUAL</a>
          </div>
          <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="#"></a>
          </div>
          <form action="/logout" method="POST" >
            @csrf
              <button type="submit" class=""> Logout</button>
           </form>
        </div>
      </div>
    </nav>
  <h2> APOTEK CITRA MEDIKA FARMA</h2>
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>id</th>
                <th>Kode Obat</th>
                <th>Nama Obat</th>
                <th>Bentuk</th>
                <th>Konsumen</th>
                <th>Harga</th>
                <th>Pilihan</th>
            </tr>
            @foreach ($dtpesan as $item)
            <tr>
                <td>{{ $item->id}}</td>
                <td>{{ $item->kode_obat}}</td>
                <td>{{ $item->nama_obat}}</td>
                <td>{{ $item->bentuk_obat}}</td>
                <td>{{ $item->konsumen}}</td>
		            <td>{{ $item->harga}}</td>
                <td>
                    <a href="{{url('edit-data',$item->id)}}"><button class="btn btn-info btn-sm">edit</button> </a>
                     <a href="{{url('hapus-data',$item->id)}}"><button class="btn btn-info btn-sm">hapus</button></a>
                </td>
            </tr>
            @endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
		            <th></th>
		            <th></th>
            </tr>
        </table>
        <div class="card-tools">
            <a href="{{url('tambah-data')}}" class="btn btn-success">Tambah<i class="fa fa_plus-square"></i></a>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>