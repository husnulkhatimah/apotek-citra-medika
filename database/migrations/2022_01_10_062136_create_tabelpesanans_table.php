<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelpesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabelpesanans', function (Blueprint $table) {
            $table->id();
            $table->string('nama_obat', 100);
            $table->string('harga_obat', 100);
            $table->string('jumlah_obat', 100);
            $table->string('tanggal', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabelpesanans');
    }
}
