<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabellaporansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabellaporans', function (Blueprint $table) {
            $table->id();
            $table->string('tanggal', 100);
            $table->string('nama_obat', 100);
            $table->string('stok_terjual', 100);
            $table->string('jumlah', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabellaporans');
    }
}
