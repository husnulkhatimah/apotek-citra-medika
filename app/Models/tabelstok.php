<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tabelstok extends Model
{
    protected $table ="tabelstoks";
    protected $primariKey ="id";
    protected $fillable = [
        'id','kode_obat','nama_obat','stok_obat'
    ];
}
