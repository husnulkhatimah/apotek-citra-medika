<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tabellaporan extends Model
{
    protected $table ="tabellaporans";
    protected $primariKey ="id";
    protected $fillable = [
        'id','tanggal','nama_obat','stok_terjual','jumlah'
    ];
}
