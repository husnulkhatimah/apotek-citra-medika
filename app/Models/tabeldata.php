<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tabeldata extends Model
{
    protected $table ="tabeldatas";
    protected $primariKey ="id";
    protected $fillable = [
        'id','kode_obat','nama_obat','bentuk_obat','konsumen','harga'
    ];
}
