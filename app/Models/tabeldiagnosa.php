<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tabeldiagnosa extends Model
{
    protected $table ="tabeldiagnosas";
    protected $primariKey ="id";
    protected $fillable = [
        'id','kode_obat','nama_obat','kategori','manfaat','warning'
    ];
}
