<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tabelpesanan extends Model
{
    protected $table ="tabelpesanans";
    protected $primariKey ="id";
    protected $fillable = [
        'id','nama_obat','harga_obat','jumlah_obat','tanggal'
    ];
}
