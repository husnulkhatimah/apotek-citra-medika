<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tabeldata;
use App\Models\tabelpesanan;

class ApotikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtpesan = tabeldata::all();
        return view('data-obat', compact('dtpesan'));
    }
    public function bb()
    {
        $dtpesan = tabeldata::all();
        return view('bb', compact('dtpesan'));
    }
    public function cc()
    {
        $dtpesan = tabelpesanan::all();
        return view('cc', compact('dtpesan'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah-data');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        tabeldata::create([
            'id' => $request->id,
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'bentuk_obat' => $request->bentuk_obat,
            'konsumen' => $request->konsumen,
            'harga' => $request->harga,
        ]);
        return redirect('pesan');

    }
    public function bbb(Request $request)
    {
        tabeldata::create([
            'id' => $request->id,
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'bentuk_obat' => $request->bentuk_obat,
            'konsumen' => $request->konsumen,
            'harga' => $request->harga,
        ]);
        return redirect('bb');

    }
    public function simpancc(Request $request)
    {
        tabelpesanan::create([
            'id' => $request->id,
            'nama_obat' => $request->nama_obat,
            'harga_obat' => $request->harga_obat,
            'jumlah_obat' => $request->jumlah_obat,
            'harga_obat' => $request->harga_obat,
            'tanggal' => $request->tanggal,
        ]);
        return redirect('cc');

    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = tabeldata::find($id);
        return view('edit-data', compact('data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = tabeldata::find($id);
        $save = $data->update([
            'id' => $request->id,
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'bentuk_obat' => $request->bentuk_obat,
            'konsumen' => $request->konsumen,
            'harga' => $request->harga,
        ]);
        if($save){
            $dtpesan = tabeldata::all();
            return redirect()->route('pesan', compact('dtpesan'));
        }
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pes = tabeldata::findorfail($id);
        $pes->delete();
        return back();
    }
    
}
