<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tabelstok;

class StokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stokobat = tabelstok::all();
        return view('stok-obat', compact('stokobat'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah-data1');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        tabelstok::create([
            'id' => $request->id,
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'stok_obat' => $request->stok_obat,
        ]);
        return redirect('stok1');

    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = tabelstok::find($id);
        return view('edit-data1', compact('data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = tabelstok::find($id);
        $save = $data->update([
            'id' => $request->id,
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'stok_obat' => $request->stok_obat,
        ]);
        if($save){
            $stokobat = tabelstok::all();
            return redirect()->route('stok1', compact('stokobat'));
        }
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pes = tabelstok::findorfail($id);
        $pes->delete();
        return back();
    }
    
}
