<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tabeldiagnosa;

class DiagnosaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diagnosa = tabeldiagnosa::all();
        return view('diagnosa', compact('diagnosa'));
    }
    public function hh()
    {
        $diagnosa = tabeldiagnosa::all();
        return view('hh', compact('diagnosa'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah-data2');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        tabeldiagnosa::create([
            'id' => $request->id,
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'kategori' => $request->kategori,
            'manfaat' => $request->manfaat,
            'warning' => $request->warning,
        ]);
        return redirect('diagnosa');

    }
    public function hhh(Request $request)
    {
        tabeldiagnosa::create([
            'id' => $request->id,
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'kategori' => $request->kategori,
            'manfaat' => $request->manfaat,
            'warning' => $request->warning,
        ]);
        return redirect('hh');

    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = tabeldiagnosa::find($id);
        return view('edit-data2', compact('data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = tabeldiagnosa::find($id);
        $save = $data->update([
            'id' => $request->id,
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'kategori' => $request->kategori,
            'manfaat' => $request->manfaat,
            'warning' => $request->warning,
        ]);
        if($save){
            $diagnosa = tabeldiagnosa::all();
            return redirect()->route('diagnosa', compact('diagnosa'));
        }
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pes = tabeldiagnosa::findorfail($id);
        $pes->delete();
        return back();
    }
    
}
