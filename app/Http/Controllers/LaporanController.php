<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tabellaporan;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laporan = tabellaporan::all();
        return view('laporan', compact('laporan'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah-data3');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        tabellaporan::create([
            'id' => $request->id,
            'tanggal' => $request->tanggal,
            'nama_obat' => $request->nama_obat,
            'stok_terjual' => $request->stok_terjual,
            'jumlah' => $request->jumlah,
        ]);
        return redirect('laporan');

    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = tabellaporan::find($id);
        return view('edit-data3', compact('data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = tabellaporan::find($id);
        $save = $data->update([
            'id' => $request->id,
            'tanggal' => $request->tanggal,
            'nama_obat' => $request->nama_obat,
            'stok_terjual' => $request->stok_terjual,
            'jumlah' => $request->jumlah,
        ]);
        if($save){
            $laporan = tabellaporan::all();
            return redirect()->route('laporan', compact('laporan'));
        }
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pes = tabellaporan::findorfail($id);
        $pes->delete();
        return back();
    }
    
}
