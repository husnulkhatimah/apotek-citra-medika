<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApotikController;
use App\Http\Controllers\StokController;
use App\Http\Controllers\DiagnosaController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\RegistrasiController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('user', function () {
    return view('home-user');
});
Route::get('log', function () {
    return view('welcome');
});
Route::get('home', function () {
    return view('home');
});
Route::get('stokobat', function () {
    return view('stok-obat');
});
Route::get('diagnosa', function () {
    return view('diagnosa');
});
Route::get('laporan', function () {
    return view('laporan');
});
Route::get('/', function () {
    return view('home-awal');
});




Route::get('pesan', [ApotikController::class,'index'])->name('pesan') ;
Route::get('bb', [ApotikController::class,'bb'])->name('bb') ;
Route::get('cc', [ApotikController::class,'cc'])->name('cc') ;
Route::get('tambah-data', [ApotikController::class ,'create'])->name('tambah-data');
Route::post('simpan', [ApotikController::class, 'store'])->name('simpan');
Route::post('simpancc', [ApotikController::class, 'simpancc'])->name('simpancc');
Route::post('bbb', [ApotikController::class, 'bbb'])->name('bbb');
Route::get('edit-data/{id}', [ApotikController::class, 'edit'])->name('edit-data');    
Route::put('/edit/{id}', [ApotikController::class, 'update'])->name('edit');    
Route::get('/hapus-data/{id}', [ApotikController::class, 'destroy'])->name('hapus-data'); 

Route::get('stok1', [StokController::class,'index'])->name('stok1') ;
Route::get('tambahdata1', [StokController::class ,'create'])->name('tambahdata1');
Route::post('simpan1', [StokController::class, 'store'])->name('simpan1');
Route::get('edit-data1/{id}', [StokController::class, 'edit'])->name('edit-data1') ;    
Route::put('/edit1/{id}', [StokController::class, 'update'])->name('edit1') ;    
Route::get('/hapus-data1/{id}', [StokController::class, 'destroy'])->name('hapus-data1') ; 

Route::get('diagnosa', [DiagnosaController::class,'index'])->name('diagnosa');
Route::get('hh', [DiagnosaController::class,'hh'])->name('hh');
Route::get('tambahdata2', [DiagnosaController::class ,'create'])->name('tambahdata2');
Route::post('simpan2', [DiagnosaController::class, 'store'])->name('simpan2') ;
Route::post('hhh', [DiagnosaController::class, 'hhh'])->name('hhh') ;
Route::get('edit-data2/{id}', [DiagnosaController::class, 'edit'])->name('edit-data2');    
Route::put('/edit2/{id}', [DiagnosaController::class, 'update'])->name('edit2');    
Route::get('/hapus-data2/{id}', [DiagnosaController::class, 'destroy'])->name('hapus-data2') ; 

Route::get('laporan', [LaporanController::class,'index'])->name('laporan');
Route::get('tambahdata3', [LaporanController::class ,'create'])->name('tambahdata3');
Route::post('simpan3', [LaporanController::class, 'store'])->name('simpan3');
Route::get('edit-data3/{id}', [LaporanController::class, 'edit'])->name('edit-data3');    
Route::put('/edit2/{id}', [LaporanController::class, 'update'])->name('edit3') ;    
Route::get('/hapus-data3/{id}', [LaporanController::class, 'destroy'])->name('hapus-data3'); 

Route::get('register', [RegistrasiController::class, 'index'])->name('register') ; 
Route::post('login', [RegistrasiController::class, 'authenticate'])->name('login') ; 
Route::post('register', [RegistrasiController::class, 'store'])->name('register') ; 
Route::post('logout', [RegistrasiController::class, 'logout'])->name('logout') ; 






